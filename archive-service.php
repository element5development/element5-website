<?php 
/*----------------------------------------------------------------*\

	CPT 'SERVICE' ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head standard">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
	<p><?php the_field($post_type.'_intro','options'); ?></p>
</header>

<main id="main-content">
	<article>
		<?php $count = 1; ?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide service-cards">
				<?php	while ( have_posts() ) : the_post(); ?>
					<?php if ( $count == 3 ) { ?>
					<article class="card">
						<?php $image = get_field('service_image', 'options'); ?>
						<?php if ( get_field('service_image', 'options') ) : ?>
						<figure>
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['small']; ?>" data-src="<?php echo $image['sizes']['large']; ?>"
								data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
								alt="<?php echo $image['alt']; ?>">
						</figure>
						<?php endif; ?>
					</article>
					<?php } ?>
					<?php get_template_part('template-parts/elements/preview-service'); ?>
				<?php $count++; ?>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="standard">
					<p>Ut oh where did our services go?</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/new-project-cta'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>