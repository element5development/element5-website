<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head standard">
	<!-- CATEGORY -->
	<?php echo get_the_category_list(); ?>
	<h1><?php the_title(); ?></h1>
	<div>
		<?php $posts = get_field('author'); ?>
		<?php if( $posts ): ?>
		<?php foreach( $posts as $post): ?>
		<?php setup_postdata($post); ?>

		<?php $image = get_field('headshot'); ?>
		<?php if ( get_field('headshot') ) : ?>
		<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>"
			data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
			alt="<?php echo $image['alt']; ?>">
		<?php endif; ?>
		<p>By <?php the_title(); ?> | <?php endforeach; ?><?php wp_reset_postdata(); ?><?php endif; ?><?php echo get_the_date('F Y');?></p>
	</div>
</header>

<section class="featured-image is-wide">
	<?php if ( has_post_thumbnail() ) : ?>
		<?php the_post_thumbnail('xlarge'); ?>
	<?php endif; ?>
</section>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
	<article>
		<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
		<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part('template-parts/sections/article/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part('template-parts/sections/article/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part('template-parts/sections/article/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part('template-parts/sections/article/media-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part('template-parts/sections/article/cover');
					elseif( get_row_layout() == 'gallery' ):
						get_template_part('template-parts/sections/article/gallery');
					elseif( get_row_layout() == 'card_grid' ):
						get_template_part('template-parts/sections/article/card-grid');
					elseif( get_row_layout() == 'post_grid' ):
						get_template_part('template-parts/sections/article/post-grid');
					endif;
				endwhile;
			?>
			<section class="author-box standard">
				<div>
					<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>"
						data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
						alt="<?php echo $image['alt']; ?>">
					<div>
						<p>Written by</p>
						<h4><?php the_author(); ?></h4>
						<p><?php $authorDesc = the_author_meta('description'); echo $authorDesc; ?></p>
						<a class="button" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">More from Me</a>
					</div>
				</div>
			</section>
			<hr>
			<?php 
				$post_url = get_permalink();
				$post_content = get_the_excerpt();
				$featured_img = get_home_url() . get_the_post_thumbnail_url();
				$post_title = get_the_title();
			?>
			<section class="social-share standard">
				<div>
					<p>Share this</p>
					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post_url; ?>" title="Share on Facebook">
						<svg>
							<use xlink:href="#facebook" />
						</svg>
					</a>
					<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $post_url; ?>" title="Share on Twitter">
						<svg>
							<use xlink:href="#twitter" />
						</svg>
					</a>
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" title="Share on Linkedin">
						<svg>
							<use xlink:href="#linkedin" />
						</svg>
					</a>
				</div>
			</section>
	</article>
	<?php else : ?>
	<article>
		<section class="editor standard">
			<?php the_content(); ?>
		</section>
		<section class="author-box standard">
			<div>
				<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>"
					data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
					alt="<?php echo $image['alt']; ?>">
				<div>
					<p>Written by</p>
					<h4><?php the_author(); ?></h4>
					<p><?php $authorDesc = the_author_meta('description'); echo $authorDesc; ?></p>
					<a class="button" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">More from Me</a>
				</div>
			</div>
		</section>
		<hr>
		<?php 
			$post_url = get_permalink();
			$post_content = get_the_excerpt();
			$featured_img = get_home_url() . get_the_post_thumbnail_url();
			$post_title = get_the_title();
		?>
		<section class="social-share standard">
			<div>
				<p>Share this</p>
				<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post_url; ?>" title="Share on Facebook">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
				<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $post_url; ?>" title="Share on Twitter">
					<svg>
						<use xlink:href="#twitter" />
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" title="Share on Linkedin">
					<svg>
						<use xlink:href="#linkedin" />
					</svg>
				</a>
			</div>
		</section>
	</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/related-posts'); ?>

<?php get_template_part('template-parts/sections/new-project-cta'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>