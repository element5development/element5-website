<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<section class="is-narrow">
			<h1>Four oh Four.</h1>
			<p>Hmmm. We don’t seem to have that page.</p>
			<a class="button" href="<?php echo get_home_url(); ?>">Back to Reality</a>
		</section>
		<div class="blob"></div>
		<div class="blob-2"></div>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>