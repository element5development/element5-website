var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$("button.activate-search").click(function () {
		$(".search-form").addClass("is-active");
		$(".search-form form input").focus();
		$("#menu-main-menu").addClass("search-active");
		$(".activate-search").addClass("search-active");
		$(".activate-menu").addClass("search-active");
		$(".primary-navigation nav > a").addClass("search-active");
	});
	$(document).click(function (event) {
		//if you click on anything except the search input, search button, or search open button, close the search
		if (!$(event.target).closest(".search-form form input, .search-form form button, button.activate-search").length) {
			$(".search-form").removeClass("is-active");
			$("#menu-main-menu").removeClass("search-active");
			$(".activate-search").removeClass("search-active");
			$(".activate-menu").removeClass("search-active");
			$(".primary-navigation nav > a").removeClass("search-active");
		}
	});

	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("button.activate-menu").click(function () {
		$("#menu-main-menu").toggleClass("is-active");
		$(".hamburger-menu").toggleClass("is-active");
	});
	$(document).click(function (event) {
		//if you click on anything except within the open menu, close the menu
		if (!$(event.target).closest("#menu-primary-navigation, button.activate-menu").length) {
			$("#menu-main-menu").removeClass("is-active");
			$(".hamburger-menu").removeClass("is-active");
		}
	});

	/*----------------------------------------------------------------*\
  	WORK FOCUS
	\*----------------------------------------------------------------*/
	$("section.post-grid .card > a").focus(function () {
		$(this).parent().addClass("focus");
	});
	$("section.post-grid .card > a").focusout(function () {
		$(this).parent().removeClass("focus");
	});

	/*----------------------------------------------------------------*\
  	MOUSE MOVE PARALLAX
	\*----------------------------------------------------------------*/
	$(document).mousemove(function (event) {
		var moveX = (($(window).width() / 2) - event.pageX) * 0.05;
		var moveY = (($(window).height() / 2) - event.pageY) * 0.05;
		var moveY2 = (($(window).width() / 2) - event.pageX) * 0.1;
		var moveX2 = (($(window).height() / 2) - event.pageY) * 0.1;
		$('.blob').css('margin-right', moveX + 'px');
		$('.blob').css('margin-top', moveY + 'px');
		$('.blob-2').css('margin-right', moveX2 + 'px');
		$('.blob-2').css('margin-top', moveY2 + 'px');
		var adjustX = (($(window).width() / 2) - event.pageX) * 0.05;
		var adjustY = (($(window).height() / 2) - event.pageY) * -0.01;
		var adjustY2 = (($(window).width() / 2) - event.pageX) * 0.05;
		var adjustX2 = (($(window).height() / 2) - event.pageY) * 0.05;
		$('.new-project .before').css('margin-left', adjustX + 'px');
		$('.new-project .before').css('margin-top', adjustY + 'px');
		$('.new-project .after').css('margin-right', adjustX2 + 'px');
		$('.new-project .after').css('margin-bottom', adjustY2 + 'px');
	});

	/*----------------------------------------------------------------*\
		AUTO FOCUS FORM 
	\*----------------------------------------------------------------*/
	if (document.getElementById("input_4_8")) {
		document.getElementById("input_4_8").focus();
	}
	if (document.getElementById("input_11_8")) {
		document.getElementById("input_11_8").focus();
	}

	/*----------------------------------------------------------------*\
		POP UP CTA
	\*----------------------------------------------------------------*/
	$("button.pop-up-cta-toggle").click(function () {
		$(".pop-up-cta").toggleClass("is-active");
	});
	$(window).scroll(function () {
		var height = $(window).height();
		var scroll = $(window).scrollTop();
		var limit = 0.6; //implies 60 vh or 60% of viewport height
		if ($(window).scrollTop() + $(window).height() == $(document).height()) {
			$(".pop-up-cta").addClass("is-hidden");
		} else if (scroll >= height * limit) {
			$(".pop-up-cta").removeClass("is-hidden");
		} else {
			$(".pop-up-cta").addClass("is-hidden");
		}
	});

});