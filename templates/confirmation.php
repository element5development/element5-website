<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>
<?php
$quote_name = $_GET['quote_name'];
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<section class="is-narrow">
			<h1><?php the_title(); ?></h1>
			<?php if ( $quote_name ) : ?>
				<p>We appreciate you contacting us, <?php echo esc_html($quote_name); ?>. One of our colleagues will get back in touch with you soon to discuss next steps.</p>
			<?php else : ?>
				<?php the_field('message'); ?>
			<?php endif; ?>
		</section>
		<div class="blob"></div>
		<div class="blob-2"></div>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>