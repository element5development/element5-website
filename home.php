<?php 
/*----------------------------------------------------------------*\

	DEFAULT POST ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head standard">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
	<p><?php the_field($post_type.'_intro','options'); ?></p>
</header>

<main id="main-content">
	<article>
		<section class="is-wide">
			<?php $post_object = get_field('featured_post','options'); ?>
			<?php if( $post_object ):  ?>
				<?php $post = $post_object; ?>
				<?php setup_postdata( $post );  ?>
					<div class="card featured">
						<div>
							<h3>Featured Insight</h3>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h2><?php the_title(); ?></h2>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- EXCERPT -->
							<p><?php the_excerpt(); ?></p>
							<!-- LINK -->
							<a class="button is-blue" href="<?php the_permalink(); ?>">Read the Rest</a>
						</div>
						<!-- IMAGE -->
						<?php if ( has_post_thumbnail() ) : ?>
						<figure>
							<?php the_post_thumbnail('large'); ?>
						</figure>
						<?php endif; ?>
					</div>
					<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</section>

		<?php query_posts( array(
			'category_name'  => 'design',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Design</h3>
					<a href="/category/design/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php get_template_part('template-parts/elements/preview-post'); ?>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<?php query_posts( array(
			'category_name'  => 'development',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Development</h3>
					<a href="/category/development/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<article class="card">
							<!-- IMAGE -->
							<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<?php the_post_thumbnail('large'); ?>
							</figure>
							<?php endif; ?>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h4><?php the_title(); ?></h4>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- LINK -->
							<a href="<?php the_permalink(); ?>"></a>
						</article>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<?php query_posts( array(
			'category_name'  => 'branding',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Branding</h3>
					<a href="/category/branding/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<article class="card">
							<!-- IMAGE -->
							<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<?php the_post_thumbnail('large'); ?>
							</figure>
							<?php endif; ?>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h4><?php the_title(); ?></h4>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- LINK -->
							<a href="<?php the_permalink(); ?>"></a>
						</article>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<?php query_posts( array(
			'category_name'  => 'strategy',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Strategy</h3>
					<a href="/category/strategy/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<article class="card">
							<!-- IMAGE -->
							<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<?php the_post_thumbnail('large'); ?>
							</figure>
							<?php endif; ?>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h4><?php the_title(); ?></h4>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- LINK -->
							<a href="<?php the_permalink(); ?>"></a>
						</article>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<?php query_posts( array(
			'category_name'  => 'marketing',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Marketing</h3>
					<a href="/category/marketing/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<article class="card">
							<!-- IMAGE -->
							<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<?php the_post_thumbnail('large'); ?>
							</figure>
							<?php endif; ?>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h4><?php the_title(); ?></h4>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- LINK -->
							<a href="<?php the_permalink(); ?>"></a>
						</article>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<?php query_posts( array(
			'category_name'  => 'culture',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Culture</h3>
					<a href="/category/culture/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<article class="card">
							<!-- IMAGE -->
							<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<?php the_post_thumbnail('large'); ?>
							</figure>
							<?php endif; ?>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h4><?php the_title(); ?></h4>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- LINK -->
							<a href="<?php the_permalink(); ?>"></a>
						</article>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<?php query_posts( array(
			'category_name'  => 'freebies',
			'posts_per_page' => 3,
		) );
		?>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<div>
					<h3>Freebies</h3>
					<a href="/category/freebies/">View All</a>
				</div>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<article class="card">
							<!-- IMAGE -->
							<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<?php the_post_thumbnail('large'); ?>
							</figure>
							<?php endif; ?>
							<!-- CATEGORY -->
							<?php echo get_the_category_list(); ?>
							<!-- HEADLINE -->
							<h4><?php the_title(); ?></h4>
							<!-- AUTHOR -->
							<p>By <?php the_author(); ?></p>
							<!-- LINK -->
							<a href="<?php the_permalink(); ?>"></a>
						</article>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>