<?php 
/*----------------------------------------------------------------*\

	NEW PROJECT CALL TO ACTION
	Commonly found right above the footer on majority of pages

\*----------------------------------------------------------------*/
?>

<section class="new-project new-project-cta">
	<div class="standard">
		<h2>Need our help on a <span>new project</span>?</h2>
		<p>Shoot us a message. We'd love to hear from you.</p>
		<a href="/get-a-quote" class="button">get a quote</a>
		<div class="before"></div>
		<div class="after"></div>
	</div>
</section>