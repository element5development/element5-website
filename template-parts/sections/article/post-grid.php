<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of previews for a specific post type

\*----------------------------------------------------------------*/
?>
<section class="post-grid is-wide <?php the_sub_field('what_post_type'); ?>-cards<?php if ( get_sub_field('leadership') ) : ?> leadership<?php endif; ?>">
	<?php $posts = get_sub_field('insights'); ?>
	<?php if( $posts ): ?>
	<?php foreach( $posts as $post): ?>
	<?php setup_postdata($post); ?>

	<?php get_template_part('template-parts/elements/preview-post'); ?>

	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>

	<?php $posts = get_sub_field('works'); ?>
	<?php if( $posts ): ?>
	<?php foreach( $posts as $post): ?>
	<?php setup_postdata($post); ?>

	<?php get_template_part('template-parts/elements/preview-work'); ?>

	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>

	<?php $posts = get_sub_field('services'); ?>
	<?php if( $posts ): ?>
	<?php foreach( $posts as $post): ?>
	<?php setup_postdata($post); ?>

	<?php get_template_part('template-parts/elements/preview-service'); ?>

	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php if( get_sub_field('show_cta') ): ?>
	<div class="card cta-card">
		<p>Looking for something like these?</p>
		<h3>Shoot us a message.</h3>
		<a class="button" href="/contact/">Get in Touch</a>
	</div>
	<?php endif; ?>
	<?php endif; ?>

	<?php $posts = get_sub_field('members'); ?>
	<?php $count = 1; ?>
	<?php if( $posts ): ?>
	<?php foreach( $posts as $post): ?>
	<?php setup_postdata($post); ?>

	<?php if ( $count == 3 ) { ?>
	<div class="card is-callout">
		<div>
			<p>Company Culture</p>
			<h2>We work hard, so we can play hard…</h2>
			<a class="button is-underlined-orange" href="/culture/">Check it out</a>
		</div>
	</div>
	<?php } ?>
	<?php if ( $count == 6 ) { ?>
	<div class="card is-callout">
		<div>
			<p>You got serious skills?</p>
			<h2>We’re always looking for talented folks.</h2>
			<a class="button is-underlined-orange" href="/careers/">View Job Openings</a>
		</div>
	</div>
	<?php } ?>

	<?php get_template_part('template-parts/elements/preview-teammember'); ?>

	<?php $count++; ?>
	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
</section>