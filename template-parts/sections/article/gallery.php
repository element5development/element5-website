<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$images = get_sub_field('gallery');
?>

<section class="gallery <?php the_sub_field('width'); ?> has-<?php the_sub_field('columns') ?>-columns <?php if( get_sub_field('show_overlay') ) : ?>overlay<?php endif; ?> <?php if( get_sub_field('two_column_offset') ) : ?>offset<?php endif; ?> <?php if( get_sub_field('flip_offset') ) : ?>flip<?php endif; ?>">
	<?php foreach( $images as $image ): ?>
	<figure>
		<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>"
			data-srcset="<?php echo $image['sizes']['medium']; ?> 350w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
			alt="<?php echo $image['alt']; ?>">
	</figure>
	<?php endforeach; ?>
</section>