<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head <?php the_field('width'); ?><?php if ( get_field('show_blobs') ) : ?> show-blobs<?php endif; ?>">
	<?php if ( get_field('title') ) : ?>
	<h1><?php the_field('title'); ?></h1>
	<?php else : ?>
	<h1><?php the_title(); ?></h1>
	<?php endif; ?>

	<?php if ( get_field('sub_title') ) : ?>
	<p><?php the_field('sub_title') ?></p>
	<?php endif; ?>

	<?php $link = get_field('button'); ?>
	<?php if ( $link ) : ?>
	<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
	<?php endif; ?>

	<?php if ( get_field('show_blobs') ) : ?>
		<div class="blob"></div>
		<div class="blob-2"></div>
	<?php endif; ?>
</header>