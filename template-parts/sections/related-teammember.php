<?php 
/*----------------------------------------------------------------*\

	RELATED TEAMMEMBER FOR SINGLE-TEAMMEMBER.PHP

\*----------------------------------------------------------------*/
?>
<section class="post-grid is-wide teammember-cards">
	<h3>Meet more E5-ers</h3>
	<?php
	$args = array(
		'post_type' => 'teammember',
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'order' => 'ASC',
		'post__not_in' => array( $post->ID ),
	);
	$loop = new WP_Query( $args );
	?>
	<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<?php get_template_part('template-parts/elements/preview-teammember'); ?>
	<?php endwhile;?>
	<?php wp_reset_query(); ?>
</section>