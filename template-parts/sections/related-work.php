<?php 
/*----------------------------------------------------------------*\

	RELATED WORK FOR SINGLE-WORK.PHP

\*----------------------------------------------------------------*/
?>
<section class="post-grid is-wide work-cards">
	<h3>More Case Studies</h3>
	<?php
	$args = array(
		'post_type' => 'work',
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'order' => 'DESC',
		'post__not_in' => array( $post->ID ),
	);
	$loop = new WP_Query( $args );
	?>
	<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<?php get_template_part('template-parts/elements/preview-work'); ?>
	<?php endwhile;?>
	<?php wp_reset_query(); ?>
</section>