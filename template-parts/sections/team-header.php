<?php 
/*----------------------------------------------------------------*\

	TEAM HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head <?php the_field('width'); ?>">
	<h1><?php the_title(); ?></h1>

	<?php if ( get_field('job_title') ) : ?>
	<p><?php the_field('job_title') ?></p>
	<?php endif; ?>
</header>