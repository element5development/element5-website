<?php 
/*----------------------------------------------------------------*\

	RELATED POSTS FOR SINGLE.PHP

\*----------------------------------------------------------------*/
?>
<?php
	$categories = get_the_category();
	$category_id = $categories[0]->cat_ID;
?>
<section class="post-grid is-wide insights-cards">
	<?php
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 3,
		'order' => 'DESC',
		'post__not_in' => array( $post->ID ),
		'cat' => $category_id,
	);
	$loop = new WP_Query( $args );
	?>
	<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<?php get_template_part('template-parts/elements/preview-post'); ?>
	<?php endwhile;?>
	<?php wp_reset_query(); ?>
</section>