<?php 
/*----------------------------------------------------------------*\

	WORK HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head work-head">
	<div class="card">
		<div>
			<h3><?php the_title(); ?></h3>
			<!-- HEADLINE -->
			<h1><?php the_field('title'); ?></h1>
		</div>
		<!-- IMAGE -->
		<?php $image = get_field('featured_image'); ?>
		<?php if ( get_field('featured_image') ) : ?>
		<figure>
			<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>"
				data-srcset="<?php echo $image['sizes']['medium']; ?> 350w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
				alt="<?php echo $image['alt']; ?>">
			<?php $blob = get_field('blob_color'); ?>
			<?php if( $blob ): ?>
			<svg id="blob" viewBox="0 0 560 473">
				<defs>
					<linearGradient x1="13.667%" y1="33.179%" x2="105.127%" y2="69.164%" id="custom-gradient">
						<stop stop-color="<?php echo $blob['blob_color_1']; ?>" offset="0%"/>
						<stop stop-color="<?php echo $blob['blob_color_2']; ?>" offset="100%"/>
					</linearGradient>
				</defs>
				<path d="M1403.268 333.338c-24.65-47.188-75.704-70.877-108.99-110.656-17.376-20.768-26.125-46.981-42.702-68.274-8.39-10.776-18.853-19.575-31.456-25.005-13.09-5.64-28.61-13.317-43.316-14.695-29.483-2.764-62.651 1.842-87.425 26.796-21.15 21.304-12.008 52.17-5.379 77.93 3.453 13.416 6.65 27.016 5.317 40.965-1.22 12.756-6.4 24.86-13.365 35.496-14.997 22.906-38.289 39.714-61.996 52.679-47.212 25.82-103.22 46.6-125.027 100.349-19.808 48.822-5.764 108.802 33.247 144.008 39.755 35.874 98.924 46.286 149.133 28.713 27.548-9.642 52.09-25.657 77.663-39.383 12.689-6.811 25.701-13.076 39.46-17.422 14.748-4.658 29.956-6.497 45.344-7.39 31.221-1.813 63.42-1.093 92.057-15.56 25.149-12.704 46.177-34.309 61.626-57.601 29.028-43.758 41.022-102.683 15.809-150.95z" transform="rotate(45 928.43 -702.156)" stroke="url(#custom-gradient)" stroke-width="15" fill="none" fill-rule="evenodd"/>
			</svg>
			<?php endif; ?>
		</figure>
		<?php endif; ?>
	</div>
</header>