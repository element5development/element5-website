<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<div>
			<h4><?php the_field('column_1_title', 'options'); ?></h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation' )); ?>
		</div>
		<div>
			<h4><?php the_field('column_2_title', 'options'); ?></h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation_2' )); ?>
		</div>
		<div>
			<h4><?php the_field('column_3_title', 'options'); ?></h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation_3' )); ?>
		</div>
		<div>
			<h4><?php the_field('column_4_title', 'options'); ?></h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation_4' )); ?>
		</div>
		<div>
			<svg>
				<use xlink:href="#logo-pentagon" />
			</svg>
			<div>
				<p>
					<strong>Element5 Digital</strong>
					<br>
					<a href="https://www.google.com/maps/place/Element5+Digital/@42.5641741,-83.1648163,15z/data=!4m5!3m4!1s0x0:0x9947a8745eb560fd!8m2!3d42.5641741!4d-83.1648163" target="_blank">
						<?php the_field('address', 'options'); ?>
					</a>
					<br>
					<a
						href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('phone', 'options')); ?>"><?php the_field('phone', 'options'); ?>
					</a>
				</p>
				<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
			</div>
		</div>
	</div>
	<div class="copyright">
		<p><span>&copy;<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved. </span>Element5
			is part of
			the <a href="https://core3solutions.com/" target="_blank">Core3 Solutions</a> family. <a
				href="/privacy-policy/">Privacy + Cookie Policy</a>. <a href="/accessibility/">Accessibility</a>.</p>
	</div>
</footer>