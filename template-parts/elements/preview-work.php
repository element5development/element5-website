<?php 
/*----------------------------------------------------------------*\

	CPT 'WORK' PREVIEW TEMPLATE
	This is the preview template for the post, for the page
	tempalte look at the single-work.php

\*----------------------------------------------------------------*/
?>
<?php $image = get_field('featured_image'); ?>
<article class="card<?php if ( get_field('featured_image') ) : ?><?php else : ?> coming-soon<?php endif; ?>">
	<!-- IMAGE -->
	<figure>
	<?php if ( get_field('featured_image') ) : ?>
		<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>"
			data-srcset="<?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
			alt="<?php echo $image['alt']; ?>">
	<?php else : ?>
		<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/work-coming-soon.png" data-src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/work-coming-soon.png"
			data-srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/work-coming-soon.png 700w, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/work-coming-soon.png 1000w, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/work-coming-soon.png 1200w"
			alt="Case Study Coming Soon">
	<?php endif; ?>
	</figure>
	<!-- HEADLINE -->
	<h3><?php the_title(); ?></h3>
	<!-- DESCRIPTION -->
	<?php the_excerpt(); ?>
	<?php if ( get_field('featured_image') ) : ?>
	<p class="is-underlined-orange">View the Work</p>
	<!-- LINK -->
	<a href="<?php the_permalink(); ?>" aria-label="<?php the_title(); ?>"></a>
	<?php endif; ?>
</article>