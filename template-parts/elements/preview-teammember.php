<?php 
/*----------------------------------------------------------------*\

	CPT 'TEAM' PREVIEW TEMPLATE
	This is the preview template for the post, for the page
	tempalte look at the single-teammember.php

\*----------------------------------------------------------------*/
?>
<article class="card">
	<!-- IMAGE -->
	<?php $image = get_field('headshot'); ?>
	<?php if ( get_field('headshot') ) : ?>
	<figure>
		<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>"
			data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"
			alt="<?php echo $image['alt']; ?>">
	</figure>
	<?php endif; ?>
	<!-- HEADLINE -->
	<h4><?php the_title(); ?></h4>
	<!-- AUTHOR -->
	<p><?php the_field('job_title'); ?></p>
	<!-- LINK -->
	<a href="<?php the_permalink(); ?>"></a>
</article>