<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>" title="Element5 Digital Home" aria-label="Element5 Digital Home">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		<button class="activate-menu" type="button" value="open-close-mobile-menu" title=“open-close-mobile-menu” aria-label="Open and Close Mobile Menu">
			<div class="hamburger-menu"></div>
		</button>
		<button class="activate-search" type="button" value="open-search" title="open-search-form" aria-label="Open search form">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
		<div class="search-form">
			<?php echo get_search_form(); ?>
		</div>
	</nav>
</div>