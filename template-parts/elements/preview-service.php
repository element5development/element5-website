<?php 
/*----------------------------------------------------------------*\

	CPT 'SERVICE' PREVIEW TEMPLATE
	This is the preview template for the post, for the page
	tempalte look at the single-service.php

\*----------------------------------------------------------------*/
?>
<article class="card">
	<!-- HEADLINE -->
	<h2><?php the_title(); ?></h2>
	<!-- DESCRIPTION -->
	<?php the_excerpt(); ?>
	<!-- WORKS LIST -->
	<?php $works = get_field('related_works'); ?>
	<?php if( $works ): ?>
	<?php foreach( $works as $work): ?>
		<?php if ( is_front_page() ) : ?>
		<?php else : ?>
		<a href="<?php the_permalink($work->ID); ?>"><?php echo get_the_title($work->ID); ?></a>
		<?php endif; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	<!-- LINK -->
	<a class="button is-underlined-orange" href="<?php the_permalink(); ?>">Learn More</a>
</article>