<?php 
/*----------------------------------------------------------------*\

	POST PREVIEW TEMPLATE
	This is the preview template for the post, for the page
	template look at the single.php

\*----------------------------------------------------------------*/
?>
<article class="card">
	<!-- IMAGE -->
	<?php if ( has_post_thumbnail() ) : ?>
	<figure>
		<?php the_post_thumbnail('large'); ?>
	</figure>
	<?php endif; ?>
	<!-- CATEGORY -->
	<?php echo get_the_category_list(); ?>
	<!-- HEADLINE -->
	<h4><?php the_title(); ?></h4>
	<!-- AUTHOR -->
	<p>By <?php the_author(); ?></p>
	<!-- LINK -->
	<a href="<?php the_permalink(); ?>"></a>
</article>