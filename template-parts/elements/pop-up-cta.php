<?php 
/*----------------------------------------------------------------*\

	LEAD GEN CTA
	button that leads to a pop up form allowing users to seek
	more information on the services on the currently viewed page

\*----------------------------------------------------------------*/
?>
<section class="pop-up-cta is-hidden">
	<button class="pop-up-cta-toggle">
		<?php if ( get_field('cta_label') ) : ?>
			<?php the_field('cta_label'); ?>
		<?php else : ?>
			I could use support like this
		<?php endif; ?>
		<svg>
			<use xlink:href="#arrow" />
		</svg>
	</button>
	<?php echo do_shortcode('[gravityform id="28" title="false" description="false"]'); ?>
</section>