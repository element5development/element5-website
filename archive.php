<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head standard">
	<?php if ( is_category() ) : ?>
	<h1><?php single_cat_title(); ?></h1>
	<?php elseif ( is_tag() ) : ?>
	<h1><?php single_tag_title(); ?></h1>
	<?php elseif ( is_author() ) : ?>
	<h1><?php the_author(); ?></h1>
	<p><?php $authorDesc = the_author_meta('description'); echo $authorDesc; ?></p>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide insights-cards">
				<?php	while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('template-parts/elements/preview-post'); ?>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>This category has no posts.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>