<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php echo 'Search results for: ' . get_search_query(); ?></h1>
</header>

<main id="main-content">
	<article>
	<?php if (have_posts()) : ?>
		<section class="standard">
		<?php	while ( have_posts() ) : the_post(); ?>
			<article class="card">
				<!-- HEADLINE -->
				<h3><?php the_title(); ?></h3>
				<!-- DESCRIPTION -->
				<?php the_excerpt(); ?>
				<!-- LINK -->
				<a class="button is-underlined-orange" href="<?php the_permalink(); ?>">Learn More</a>
			</article>
		<?php endwhile; ?>
		</section>
	<?php else : ?>
		<section class="standard">
			<article>
				<p>We cannot find anything for "<?php echo(get_search_query()); ?>".</p>
				<?php get_search_form( $echo ); ?>
			</article>
		</section>
	<?php endif; ?>
	<?php clean_pagination(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>