<?php 
/*----------------------------------------------------------------*\

	CPT 'WORK' ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head standard">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
	<p><?php the_field($post_type.'_intro','options'); ?></p>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-wide work-cards">
				<?php	while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('template-parts/elements/preview-work'); ?>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="standard">
					<p>Ut oh where did our work go?</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/new-project-cta'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>