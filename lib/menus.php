<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'footer_navigation' => __( 'Footer Menu 1' ),
		'footer_navigation_2' => __( 'Footer Menu 2' ),
		'footer_navigation_3' => __( 'Footer Menu 3' ),
		'footer_navigation_4' => __( 'Footer Menu 4' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );