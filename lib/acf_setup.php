<?php
/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Archives',
		'menu_title'	=> 'Archives',
		'menu_slug' 	=> 'archives-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Company Info',
		'menu_title'	=> 'Company Info',
		'menu_slug' 	=> 'company-info',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
/*----------------------------------------------------------------*\
	RESET ACF POSITIONS
\*----------------------------------------------------------------*/
// function prefix_reset_metabox_positions(){
//   delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_post' );
//   delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_page' );
//   // delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_YOUR_CPT_SLUG' );
// }
// add_action( 'admin_init', 'prefix_reset_metabox_positions' );
?>